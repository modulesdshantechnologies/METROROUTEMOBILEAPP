var electionapp=angular.module('starter.controllers', []);

electionapp.controller('MenuCtrl', function($scope, $ionicSideMenuDelegate,$ionicScrollDelegate) {
  $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.scrollTop = function() {
        $ionicScrollDelegate.scrollTop();
     };
});

electionapp.controller('slideCtrl', function($scope, $ionicSlideBoxDelegate) {
$scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  }

});


electionapp.controller('modalwindowCtrl', ['$scope', '$ionicModal', '$ionicSlideBoxDelegate', function ($scope, $ionicModal, $ionicSlideBoxDelegate) {

  	$scope.aImages = [{
      	'src' : 'img/ach2.jpg',
      	'msg' : 'Swipe me to the left. Tap/click to close'
    	}, {
        'src' : 'https://ionicframework.com/img/ionic_logo.svg',
        'msg' : ''
      }, {
        'src' : 'https://ionicframework.com/img/homepage/phones-weather-demo@2x.png',
        'msg' : ''
    }];

    $ionicModal.fromTemplateUrl('image-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });

    $scope.openModal = function() {
      $ionicSlideBoxDelegate.slide(0);
      $scope.modal.show();
    };

    $scope.closeModal = function() {
      $scope.modal.hide();
    };

    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hide', function() {
      // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
      // Execute action
    });
    $scope.$on('modal.shown', function() {
      console.log('Modal is shown!');
    });

    // Call this functions if you need to manually control the slides
    $scope.next = function() {
      $ionicSlideBoxDelegate.next();
    };

    $scope.previous = function() {
      $ionicSlideBoxDelegate.previous();
    };

  	$scope.goToSlide = function(index) {
      $scope.modal.show();
      $ionicSlideBoxDelegate.slide(index);
    }

    // Called each time the slide changes
    $scope.slideChanged = function(index) {
      $scope.slideIndex = index;
    };
  }
]);






electionapp.controller('PopupCtrl',function($scope, $ionicPopup, $timeout) {

// Triggered on a button click, or some other target
$scope.showPopup = function() {
  $scope.data = {};

  // An elaborate, custom popup
  var myPopup = $ionicPopup.show({
    template: '<input type="password" ng-model="data.wifi">',
    title: 'Enter Wi-Fi Password',
    subTitle: 'Please use normal things',
    scope: $scope,
    buttons: [
      { text: 'Cancel' },
      {
        text: '<b>Save</b>',
        type: 'button-positive',
        onTap: function(e) {
          if (!$scope.data.wifi) {
            //don't allow the user to close unless he enters wifi password
            e.preventDefault();
          } else {
            return $scope.data.wifi;
          }
        }
      }
    ]
  });

  myPopup.then(function(res) {
    console.log('Tapped!', res);
  });

  $timeout(function() {
     myPopup.close(); //close the popup after 3 seconds for some reason
  }, 3000);
 };

 // A confirm dialog
 $scope.showConfirm = function() {
   var confirmPopup = $ionicPopup.confirm({
     title: 'Consume Ice Cream',
     template: 'Are you sure you want to eat this ice cream?'
   });

   confirmPopup.then(function(res) {
     if(res) {
       console.log('You are sure');
     } else {
       console.log('You are not sure');
     }
   });
 };

 // An alert dialog
 $scope.showAlert = function() {
   var alertPopup = $ionicPopup.alert({
     title: 'Don\'t eat that!',
     template: 'It might taste good'
   });

   alertPopup.then(function(res) {
     console.log('Thank you for not eating my delicious ice cream cone');
   });
 };
});





electionapp.controller('toastCtrl', ['$scope', 'ionicToast', function($scope, ionicToast) {
  //code here
  $scope.showToast = function(){
//  <!-- ionicToast.show(message, position, stick, time); -->
    ionicToast.show('This is a toast at the top.', 'top', true, 2500);
  };
  $scope.hideToast = function(){
    ionicToast.hide();
  };

}]);


electionapp.controller('MapCtrl', function($scope, $state,$cordovaGeolocation ) {
  var options = {timeout: 10000, enableHighAccuracy: true};

   $cordovaGeolocation.getCurrentPosition(options).then(function(position){

      var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      var mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

      // map pointer
      google.maps.event.addListenerOnce($scope.map, 'idle', function(){
          console.log('Here is the idle.....')
          var marker = new google.maps.Marker({
              map: $scope.map,
              animation: google.maps.Animation.DROP,
              position: latLng
          });

          var infoWindow = new google.maps.InfoWindow({
              content: "Here I am!"
          });

          google.maps.event.addListener(marker, 'click', function () {
              infoWindow.open($scope.map, marker);
          });

        });

    }, function(error){
      console.log("Could not get location");
    });


});



 electionapp.controller('timepickCtrl', function ($scope, ionicTimePicker) {

   var ipObj1 = {
     callback: function (val) {      //Mandatory
       if (typeof (val) === 'undefined') {
         console.log('Time not selected');
       } else {
         var selectedTime = new Date(val * 1000);
         console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');
       }
     },
     inputTime: 50400,   //Optional
     format: 12,         //Optional
     step: 15,           //Optional
     setLabel: 'Set2'    //Optional
   };

    $scope.openTimePicker = function(){
      ionicTimePicker.openTimePicker(ipObj1);
    };

 });








electionapp.controller('datepickCtrl', function ($scope) {
     /* var ipObj1 = {
      callback: function (val) {  //Mandatory
        console.log('Return value from the datepicker popup is : ' + val, new Date(val));
      },
      disabledDates: [            //Optional
        new Date(2016, 2, 16),
        new Date(2015, 3, 16),
        new Date(2015, 4, 16),
        new Date(2015, 5, 16),
        new Date('Wednesday, August 12, 2015'),
        new Date("08-16-2016"),
        new Date(1439676000000)
      ],
      from: new Date(2012, 1, 1), //Optional
      to: new Date(2016, 10, 30), //Optional
      inputDate: new Date(),      //Optional
      mondayFirst: true,          //Optional
      disableWeekdays: [0],       //Optional
      closeOnSelect: false,       //Optional
      templateType: 'popup'       //Optional
    };

    $scope.openDatePicker = function(){
      ionicDatePicker.openDatePicker(ipObj1);
    };*/

    $scope.currentDate = new Date();
    $scope.minDate = new Date(2105, 6, 1);
    $scope.maxDate = new Date(2015, 6, 31);

    $scope.datePickerCallback = function (val) {
        if (!val) {
            console.log('Date not selected');
        } else {
            console.log('Selected date is : ', val);
        }
    };

  });

electionapp.controller("chartCtrl", function($scope) {

    /*$scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
    $scope.series = ['Series A', 'Series B'];
    $scope.data = [
        [65, 59, 80, 81, 56, 55, 40],
        [28, 48, 40, 19, 86, 27, 90]
    ];

    $scope.onClick = function (points, evt) {
        console.log(points, evt);
      };
      $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
      $scope.options = {
        scales: {
          yAxes: [
            {
              id: 'y-axis-1',
              type: 'linear',
              display: true,
              position: 'left'
            },
            {
              id: 'y-axis-2',
              type: 'linear',
              display: true,
              position: 'right'
            }
          ]
        }
      };*/

      $scope.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales", "Tele Sales", "Corporate Sales"];
          $scope.data = [300, 500, 100, 40, 120];
          $scope.type = 'polarArea';

          $scope.toggle = function () {
            $scope.type = $scope.type === 'polarArea' ?
              'pie' : 'polarArea';
          };

});


electionapp.controller('HomeTabCtrl', function($scope, $ionicModal) {

    // Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('templates/achpage1.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) { $scope.modal = modal; });

    $scope.modal2 = $ionicModal.fromTemplate('<div class="modal"><header class="bar bar-header bar-positive"> <h1 class="title">I\'m A Modal</h1><div class="button button-clear" ng-click="modal2.hide()"><span class="icon ion-close"></span></div></header><content has-header="true" padding="true"><p>This is a modal</p></content></div>', {
      scope: $scope,
      animation: 'slide-left-right'
    });


});










electionapp.controller('AppCtrl', function($scope, $ionicModal, $timeout,$state) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  $scope.loginmenu = function(){
  console.log("hai");
  $state.go('election');
  };


  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})


electionapp.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'MEAN Stack', id: 0,imgName:'meanStack.jpg' },
    { title: 'Digital marketing', id: 1,imgName:'digitalmarketing.jpg' },
    { title: 'BFSI', id: 2 ,imgName:'bfsilogo.png'},
    { title: 'Vmware vsphere', id: 3 ,imgName:'vmwarevsphere.png'},

  ];
})

electionapp.controller('PlaylistCtrl', function($scope, $stateParams) {

$scope.courseDetails = [
    {
    "title": 'MEAN Stack',
    "id": 1 ,
    "courseDesp":"Learn all of the different aspects of full stack JavaScript development using the MEAN stack. Which involves understanding of MongoDB, Express, AngularJS and Node.js."
                     + "Throughout this course we'll show you how to use each of these technologies, and how to use them together in Real world applications.",
    "courseFeatures":["Build and test your own MEAN applications",
    "comphernsive courseware",
    "Hands-on practice session"],
    "targetedAudiance":["Anyone who wants to learn to codeing",
    "Anyone who wants to generate new income streams",
    "Anyone who wants to build websites, job seekers",
    "Anyone who wants to become financially independent",
    "Anyone who wants to start their own business or become freelance"],
    "fileName":"Download File (4)(1).pdf",
    "imgName":'meanStack.jpg',
    "coursePdfName":  "MeanStack"
},
 {
    "title": 'Digital marketing',
    "id": 2 ,
    "courseDesp":"Digital Marketing Training In Bangalore Magadi Road  Marathahalli & Btm"+
"Layout Course Contents. Start your career with DIGITAL MARKETING course that gets you a job of upto 4 lakhs",
    "courseFeatures":["Build and test your own Website",
    "100's of free tools with job assitance",
    "comphernsive courseware",
    "Hands-on practice session",
    "100% placement graunted"],

    "targetedAudiance":["Anyone who wants to generate new income streams",
    " Anyone who wants to build websites, job seekers",
    "Anyone who wants to become financially independent",
    "Anyone who wants to start their own business or become freelance"],
     "fileName":"Download File (2).pdf",
     "imgName":'digitalmarketing.jpg',
     "coursePdfName":  "digitalMarketing"

},

{
    "title": 'BFSI ',
    "id": 3 ,
    "courseDesp":"   Banking & Financial system of the country plays a substantial role in promoting the long"
                                                                  +"term growth of the economy. The major segments of the Industry are Banking, Insurance and Mutual Funds."
                                                                  +"Dshan technologies provides government added free training for 45 days to get the professional domain knowkegde",
    "courseFeatures":["Join 45 days free training and assesment program for account executive",
    "Hands-on practice session tools like Tally 9 and odoo",
    "Get government certificate and skill card",
    "100% placement guaranteed as a part of program"],
    "targetedAudiance":[" Graduate of commerce or alied subject",
    "Diploma in commercial practice",
    " Drop outs ofGraduate of commerce"],
    "fileName":"BFSI_Syllabus.pdf",
    "imgName":'bfsilogo.png',
    "coursePdfName":  "BFSISyllabus"
},
{
    "title": 'Vmware Vsphere',
    "id": 4 ,
    "courseDesp":" This hands-on training course explores installation, configuration,and management of VMware vSphere®, which consists of VMware ESXi™ and VMware® vCenter Server™."+
                                                                 +" This course is based on versions Of ESXi 6.0 and vCenter Server 6.0. Students who complete this course can enroll in several more advanced vSphere courses.",
    "courseFeatures":["Deploy and Manage an ESXi host using vCenter Server",
    "Comphernsive courseware",
    "Hands-on practice session"],
    "targetedAudiance":["Anyone who wants to build websites, job seekers",
    "Anyone who wants to become financially independent",
    "Anyone who wants to start their own business or become freelance"],
    "fileName":"EDU_DATASHEET VMware AirWatch Configure and Manage.pdf",
    "imgName":'vmwarevsphere.png',
     "coursePdfName":  "vmware.pdf"
}

];
$scope.courseFeaturesDetails=$scope.courseDetails[$stateParams.playlistId]


});
